package pt.com.scrumify.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetApproval;
import pt.com.scrumify.database.entities.Year;

public class TimesheetApprovalView {
   
   @Getter
   @Setter
   private Year year;  
   
   @Getter
   @Setter
   private Area area;
   
   @Getter
   @Setter
   private Contract contract;
   
   @Getter
   @Setter
   private Resource resource;
   
   @Getter
   @Setter
   private Integer month;
   
   @Getter
   @Setter
   private Integer fortnight;
   
   @Getter
   @Setter
   private Integer balance = 0;
   
   @Getter
   @Setter
   private Integer hours;
   
   @Getter
   @Setter
   private Integer hoursF1;
   
   @Getter
   @Setter
   private Integer hoursF2;
   
   @Getter
   @Setter
   private Integer totalHours = 0;
   
   @Getter
   @Setter
   private Integer analysis;
   
   @Getter
   @Setter
   private Integer design;
   
   @Getter
   @Setter
   private Integer development;
   
   @Getter
   @Setter
   private Integer test;
   
   @Getter
   @Setter
   private Integer other;
   
   @Getter
   @Setter
   private Integer absence;
   
   @Getter
   @Setter
   private Integer compensation;
   
   @Getter
   @Setter
   private Integer holiday;
   
   @Getter
   @Setter
   private Integer vacation;

   @Getter
   @Setter
   private boolean approved = false;
   
   @Getter
   @Setter
   private List<TimesheetApproval> approvals = new ArrayList<>();
   
   public TimesheetApprovalView() {
      super();
   }
   
   public TimesheetApprovalView(int month) {
      super();
      this.month = month;
      this.hours = 0;
      this.hoursF1 = 0;
      this.hoursF2 = 0;
      this.analysis = 0;
      this.design = 0;
      this.development = 0;
      this.test = 0;
      this.other = 0;
      this.absence = 0;
      this.compensation = 0;
      this.holiday = 0;
      this.vacation = 0;
   }
   
   public TimesheetApprovalView(Timesheet timesheet) {
      super();
      
      this.resource = timesheet.getResource();
      this.year = timesheet.getStartingDay().getYear();
      this.month = timesheet.getStartingDay().getMonth();
      this.fortnight = timesheet.getStartingDay().getFortnight();
      this.approved = timesheet.getApproved();
      this.approvals = timesheet.getApprovals();
   }
   
   public TimesheetApprovalView(TimesheetApproval approval) {
      super();

      this.contract = approval.getContract();
      this.resource = approval.getResource();
      this.fortnight = approval.getFortnight();
      this.month = approval.getMonth();
      this.year = approval.getYear();
      this.analysis = approval.getAnalysis();
      this.design = approval.getDesign();
      this.development = approval.getDevelopment();
      this.test = approval.getTest();
      this.other = approval.getOther();
      this.absence = 0;
      this.compensation = 0;
      this.holiday = 0;
      this.vacation = 0;
   }
   
   public TimesheetApprovalView(Contract contract, Year year, int month,long analysis,long design,long development,long test,long other, long absence,long compensation,long holiday,long vacation) {
      super();
      this.month = month;
      this.year = year;
      this.contract = contract;
      this.hoursF1 = 0;
      this.hoursF2 = 0;
      this.analysis = (int)analysis;
      this.design = (int)design;
      this.development = (int)development;
      this.test = (int)test;
      this.other = (int)other;
      this.hours =this.analysis+this.design+this.development+this.test+ this.other;
      this.absence = (int)absence;
      this.compensation = (int)compensation;
      this.holiday = (int)holiday;
      this.vacation = (int)vacation;
      
   }
   
   public TimesheetApprovalView(Contract contract , Year year, int month,long analysis,long design,long development,long test,long other) {
      super();
      this.month = month;
      this.year = year;
      this.contract = contract;
      this.hoursF1 = 0;
      this.hoursF2 = 0;
      this.analysis = (int)analysis;
      this.design = (int)design;
      this.development = (int)development;
      this.test = (int)test;
      this.other = (int)other;
      this.hours =this.analysis+this.design+this.development+this.test+ this.other;
      this.absence = 0;
      this.compensation = 0;
      this.holiday = 0;
      this.vacation = 0;
   }
}