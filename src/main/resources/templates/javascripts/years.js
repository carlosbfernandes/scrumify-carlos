$(document).ready(function() {
   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function changeActive(checkbox, id) {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_YEARS_UPDATE}}]] + '/' + id + '/' + checkbox.checked;
   ajaxget(url, "table[id='years']", function() {           
		initialize();   
   });
}

function formSubmission() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_YEARS_SAVE}}]];
   ajaxpost(url, $("#years-form").serialize(), "", false, function() {
      initialize();
   });
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  name : {
            identifier : 'id',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}