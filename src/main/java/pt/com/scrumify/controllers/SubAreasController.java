package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.services.AreaService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.entities.SubAreaView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class SubAreasController {
   @Autowired
   private AreaService areaService;
   @Autowired
   private SubAreaService subAreaService;

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
   
   /*
    * SUBAREAS
    */
   @GetMapping(value = ConstantsHelper.MAPPING_SUBAREAS)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SUBAREAS_READ + "')")
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.getAll());
      
      return ConstantsHelper.VIEW_SUBAREAS_READ;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_SUBAREAS_CREATE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SUBAREAS_CREATE + "')")
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, new SubAreaView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, this.areaService.listAll());
      
      return ConstantsHelper.VIEW_SUBAREAS_CREATE;
   }

   @GetMapping(value = ConstantsHelper.MAPPING_SUBAREAS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_SUBAREA)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SUBAREAS_UPDATE + "')")
   public String update(Model model, @PathVariable int subarea) {
      SubAreaView subAreaView = new SubAreaView(this.subAreaService.getOne(subarea));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, subAreaView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, this.areaService.listAll());
      
      return ConstantsHelper.VIEW_SUBAREAS_UPDATE;
   }   
   
   @PostMapping(value = ConstantsHelper.MAPPING_SUBAREAS_SAVE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_SUBAREAS_UPDATE + "')")
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA) @Valid SubAreaView subAreaView, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, subAreaView);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, this.areaService.listAll());
         
         return ConstantsHelper.VIEW_SUBAREAS_UPDATE;
      }
      
      this.subAreaService.save(subAreaView.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_SUBAREAS;
   }
   
}