$(document).ready(function() {
	   initialize();
	  
	});
	        
function initialize() {
	var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	var minDate = $("input[type=hidden][id='minDate']").val();
	var maxDate = $("input[type=hidden][id='maxDate']").val();

	calendar('day', 'date', json, minDate, maxDate);

	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function createAbsence(){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_CREATE}}]] ;
	   
	ajaxget(url,  "div[id='modal']", function() {
        initialize();
    }, true);
}



function deleteAbsence(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
    $("#modal-confirmation")
	.modal({closable: false,
   		onApprove: function() {
   			ajaxget(url, "div[id='absences-list']", function() {
   	        initialize();
   	       }, false);
   		},
   		onDeny: function() {
   		}
	})
	.modal('show');  
}

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_SAVE}}]];
   ajaxpost(url, $("#absences-form").serialize(), "div[id='absences-list']", true, function() {
	   initialize();
   });
}

function formValidation() {
	$("#absences-form").form({
	      on : 'blur',
	      inline : true,
	      fields : {
	    	  type : {
	            identifier : 'type.id',
	            rules : [ {
	               type : 'empty',
	               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	            } ]
	         },
	         day : {
	            identifier : 'day',
	            rules : [ {
	               type : 'empty',
	               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	            }]
	         },
	         hours : {
	            identifier : 'hours',
	            rules : [ {
	            	   type : 'empty',
		               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
		            } ]
		         },
	         timesheet : {
	            identifier : 'day',
	            rules : [ {
		            type : 'timesheet',
		            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
		            value: "input[id='input-day']"			            	
            	}]
              }
	      },
		      
		      onSuccess : function(event) {
		         event.preventDefault();
		         
		         formSubmission();
		      }
	   });
}

function editAbsence(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ABSENCES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
    
    ajaxget(url, "div[id='modal']", function() {
        initialize();
    }, true);
}