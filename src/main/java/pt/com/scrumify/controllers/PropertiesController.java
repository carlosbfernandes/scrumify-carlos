package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.services.PropertyService;
import pt.com.scrumify.entities.PropertyView;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class PropertiesController {
   
   @Autowired
   private PropertyService propertiesService;   
     
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * LIST
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROPERTIES_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROPERTIES)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES , propertiesService.getAll());

      return ConstantsHelper.VIEW_PROPERTIES_READ;
   }
   
   /*
    * CREATE
    */   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROPERTIES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROPERTIES_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY, new PropertyView());
      
      return ConstantsHelper.VIEW_PROPERTIES_SAVE;
   }
   
   /*
    * SAVE
    */ 
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROPERTIES_UPDATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_PROPERTIES_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY) @Valid PropertyView propertyView) {

      propertiesService.save(propertyView.translate());
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_PROPERTIES;
   }
   
   /*
    * UPDATE
    */ 
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_PROPERTIES_UPDATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_PROPERTIES_UPDATE + ConstantsHelper.MAPPING_PARAMETER_ID )
   public String update(Model model, @PathVariable Integer id) {

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY, new PropertyView(propertiesService.getOne(id)));
      return ConstantsHelper.VIEW_PROPERTIES_SAVE;
   }
}