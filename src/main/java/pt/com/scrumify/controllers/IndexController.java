package pt.com.scrumify.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;


@Controller
public class IndexController {
   @Autowired
   private ContractService contractsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private WorkItemService workItemsService;
   
   /*
    * List Unapproved Resources
    */
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCES_UNAPPROVED_AJAX)
   public String unapproved(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_UNAPPROVED_RESOURCES, this.resourcesService.getByApproved(false));

      return ConstantsHelper.VIEW_FRAGMENT_UNAPPROVED_RESOURCES;
   }

   /*
    * List Resources with TimesSheet to review  
    */
   @GetMapping(value = ConstantsHelper.MAPPING_RESOURCES_TIMESHEET_UNREVIEWED_AJAX)
   public String unreviewedtimesheet(Model model) {
      List<Resource> resources = new ArrayList<>();       
      List<Team> teams = teamsService.getByMemberAndReviewerOrApprover(ResourcesHelper.getResource());
      
      if(!teams.isEmpty())
         resources = resourcesService.getResourcesWithTimesheetUnreviewed(resourcesService.getByTeams(teams));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES_UNREVIEWED_TIMESHEETS, resources);

      return ConstantsHelper.VIEW_FRAGMENT_UNREVIEWED_TIMESHEETS;
   }
   
   /*
    * List User Teams
    */
   @GetMapping(value = ConstantsHelper.MAPPING_USER_TEAMS_AJAX )
   public String myteams(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERTEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_FRAGMENT_USER_TEAMS;
   }
   
   /*
    * List WorkItems
    */
   @GetMapping(value = ConstantsHelper.MAPPING_WORKITEMS_LIST_AJAX)
   public String listWorkItems(Model model) {      
      Resource resource = ResourcesHelper.getResource();
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, workItemsService.getCreatedByUser(resource));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MYWORKITEMS, workItemsService.getWorkItemsByUser(resource));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMSNOTASSIGNED, workItemsService.getWorkItemsNotAssigendByTeam(teamsService.getByMember(resource)));

      return ConstantsHelper.MAPPING_WORKITEMS_LIST;
   }
   
   /*
    * List Contracts
    */
   @GetMapping(value = ConstantsHelper.MAPPING_CONTRACTS_LIST_AJAX)
   public String listContracts(Model model) {      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS , contractsService.getOpenContractsByResource(ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_CONTRACTS_LIST;
   }
     
}