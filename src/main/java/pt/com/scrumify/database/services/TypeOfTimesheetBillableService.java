package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;

public interface TypeOfTimesheetBillableService {
   TypeOfTimesheetApproval getOne(Integer id);
   List<TypeOfTimesheetApproval> listAll();
}