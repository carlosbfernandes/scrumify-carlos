package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Financial;
import pt.com.scrumify.database.repositories.FinancialRepository;

@Service
public class FinancialServiceImpl implements FinancialService {
   
   @Autowired
   private FinancialRepository repository;

   @Override
   public Financial getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public List<Financial> getByContractAndType(Contract contract, String type) {
      return repository.getByContractAndType(contract, type);
   }

   @Override
   public Financial save(Financial entity) {
      return repository.save(entity);
   }

   @Override
   public List<Financial> save(List<Financial> entities) {
      return repository.saveAll(entities);
   }

   @Override
   public List<Financial> listAll() {
      return repository.findAll();
   }
}