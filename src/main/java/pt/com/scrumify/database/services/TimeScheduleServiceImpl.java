package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimeScheduleRepository;

@Service
public class TimeScheduleServiceImpl implements TimeScheduleService {
   @Autowired
   private TimeScheduleRepository repository;

   @Override
   public List<TimeSchedule> getByYearMonthAndFortnight(Year year, Integer month, Integer fortnight, Schedule schedule) {
      return repository.getByYearMonthAndFortnight(year, month, fortnight, schedule);
   }

   @Override
   public TimeSchedule getByDay(Time day, Schedule schedule) {
      return repository.findByPkTimeAndPkSchedule(day, schedule);
   }
}