$(document).ready(function() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL}}]];	   
  var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
  ajaxtab(true, url, true, function(settings) {
    switch(settings.urlData.tab) {
      case "contracts":
        getcontracts();
        break;
      case "resources":
      default:
        gettimesheets();
        break;
    }
  });

  initialize();
});
	        
function initialize() {
  $('.ui.dropdown').dropdown({
    ignoreDiacritics: true,
    sortSelect: true,
    fullTextSearch:'exact'
  });
  $('.ui.mini').popup({
    position: 'top center'
  });
}

function getcontracts() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_CONTRACTS}}]];
    
  ajaxget(url, "div[data-tab=contracts]", function() {
    initialize();
  });
}

function gettimesheets() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_RESOURCES}}]];
  
  ajaxget(url, "div[data-tab=resources]", function() {
    initialize();
  });
}
	
function updatetimesheet(timesheet) {
  var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + timesheet;
  
  ajaxget(url, "div[id='modal']", function() {
    initialize();
  }, true);
}

function addNewRow(){
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_ADDROW}}]];
  
  ajaxpost(url, $("#timesheetapproval-form").serialize(), "div[id='modal']", false, function() {
    initialize();
  });	
}

function approve() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_APPROVE}}]];
	
  $('#timesheetapproval-form').submit(function(event) {
    event.preventDefault();
    
    ajaxpost(url, $("#timesheetapproval-form").serialize(), "table[id='resourcehours']", true, function() {
      initialize();
    });	    
  });
}

function reject() {	
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_REJECT}}]];
  
  $('#timesheetapproval-form').submit(function(event) {
    event.preventDefault();
    
    ajaxpost(url, $("#timesheetapproval-form").serialize(), "table[id='resourcehours']", true, function() {
      initialize();
    });	    
  });
}

function remove(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_DELETEROW}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  
  ajaxget(url, "div[id='modal']", function() {
    initialize();
  }, false);
}

function save() {
  $('#timesheetapproval-form').submit(function(event) {
    event.preventDefault();
    
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SAVE}}]];
    
    ajaxpost(url, $("#timesheetapproval-form").serialize(), "table[id='resources']", false, function() {
      initialize();
    });     
  });
}

function updateContractOrAbsence(index, code, contract, description) {
  $("input[id='approvals" + index + ".code']").val(code);
  $("input[id='approvals" + index + ".contract']").val(contract);
  $("input[id='approvals" + index + ".description']").val(description);
  $("label[id='approvals[" + index + "].description']").html(description);
};

function search() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_APPROVAL_SEARCH}}]];
  var tab = "";
  
  if (window.location.pathname.endsWith("contracts")) {
    tab = "div[data-tab=contracts]";
  }
  else {
    tab = "div[data-tab=resources]";
  }
  
  ajaxpost(url, $("#filter-form").serialize(), tab, false, function() {
    initialize()
  });
}