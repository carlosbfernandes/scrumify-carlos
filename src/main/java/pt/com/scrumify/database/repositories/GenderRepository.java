package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Gender;

public interface GenderRepository extends JpaRepository<Gender, Integer> {
}