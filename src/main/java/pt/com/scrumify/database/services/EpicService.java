package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.entities.ReportView;

public interface EpicService {
   Epic getOne(Integer id);
   Epic save(Epic epic);
   Epic saveTeam(Epic epic, Team team);
   
   List<Epic> getAll();
   List<Epic> getAllExceptPresent(Epic epic);   
   List<Epic> getAllByResource(Resource resource);
   List<Epic> getToReport(ReportView reportView);
   List<Epic> getByTeam(Team team);
   List<Epic> getByTeams(List<Team> teams);
   List<Epic> getByStatusesAndResource(Resource resource, List<Integer> statuses);
   List<Epic> getByTeamId(Integer team);
   List<Epic> getByTeamAndNotFinalStatus(Team team);
   List<Epic> findEpicsWithoutUserStories(Team team);
   
   int closestValue (List<Integer> list, int value);
   void updateTeam(Epic epicsaved);
   
   WorkItemStatus getOneStatus(Integer id);
   String formatEpicNumber(Epic epic);
}