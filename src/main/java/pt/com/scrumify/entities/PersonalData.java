package pt.com.scrumify.entities;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.helpers.ConstantsHelper;

public class PersonalData {   

   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(min=3, max=3, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String abbreviation;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Gender gender = new Gender(Gender.MALE);
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Email(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMAIL + "}")
   private String email;
   
   @Getter
   @Setter
   @Length(min=0, max=20, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String phone;
   
   @Getter
   @Setter
   @Length(min=0, max=20, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String mobile;
   
   @Getter
   @Setter
   @Length(min=0, max=5, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String extension;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Avatar avatar = new Avatar();

   public PersonalData(Resource resource) {
      super();

      if (resource != null) {
         this.setId(resource.getId());
         this.setAbbreviation(resource.getAbbreviation());
         this.setName(resource.getName());
         this.setGender(resource.getGender());
         this.setEmail(resource.getEmail());
         this.setAvatar(resource.getAvatar());
         this.setPhone(resource.getPhone());
         this.setMobile(resource.getMobile());
         this.setExtension(resource.getExtension());
      }
   }

   public Resource translate() {
      Resource resource = new Resource();
      
      resource.setId(this.getId());
      resource.setAbbreviation(this.getAbbreviation());
      resource.setName(this.getName());
      resource.setGender(this.getGender());
      resource.setEmail(this.getEmail());
      resource.setAvatar(this.getAvatar());
      resource.setPhone(this.getPhone());
      resource.setMobile(this.getMobile());
      resource.setExtension(this.getExtension());

      return resource;
   }
}