package pt.com.scrumify.database.services;

import java.util.List;
import java.util.Map;

import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Year;

public interface CompensationDayService {
   CompensationDay getOne(Integer id);
   void save(CompensationDay compensationDay);
	
   List<CompensationDay> getAll();
   List<CompensationDay> getByYearAndMonth(Year year, Integer month);
   void delete(CompensationDay compensationDay);
   List<CompensationDay> listdays(CompensationDay compensationDay);
   void saveAll(List<CompensationDay> compensations);
   Map<Integer, List<CompensationDay>> getAllInMap();
   
   Long getResourceHoursByTimesheet(Integer year, Integer month, Integer fortnight, Schedule schedule);
}