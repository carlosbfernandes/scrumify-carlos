package pt.com.scrumify.database.services;

import java.util.Date;
import java.util.List;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;

public interface WorkItemWorkLogService {
   WorkItemWorkLog getOne(Integer id);
   WorkItemWorkLog getByIdAndResource(Integer id, Resource resource);
   WorkItemWorkLog save(WorkItemWorkLog entity);
   
   List<WorkItemWorkLog> getAll();
   List<WorkItemWorkLog> getByContract(Contract contract);
   List<WorkItemWorkLog> getByResource(Resource resource);
   List<WorkItemWorkLog> getByResourceAndContract(Resource resource, Contract contract);
   List<WorkItemWorkLog> getByYearAndMonthAndResourceAndFortnighOrderByDate(Year year, Integer month, Resource resource, Integer fortnight);
   List<WorkItemWorkLog> getByYearAndMonthAndResourceOrderByDate(Year year, Integer month, Resource resource);
   List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnighOrderByDate(Year year, Integer month, List<Resource> resources, Integer fortnight);
   List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnighAndContractOrderByDate(Year year, Integer month, Resource resource, Integer fortnight, Contract contract);
   
   void saveAll(List<WorkItemWorkLog> workLogs);
   
   int getTimeSpentDuringInterval(Contract contract, Date start, Date end);
   int getTimespentOnWorkitemAndNotWorklog(WorkItem workItem, Integer worklog);
   
   void delete(WorkItemWorkLog workItemWorkLog);
}