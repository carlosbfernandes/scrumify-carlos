package pt.com.scrumify.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import pt.com.scrumify.helpers.ConstantsHelper;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
   @Autowired
   private UserDetailsService userDetailsService;
   
   @Override
   public void configure(AuthenticationManagerBuilder builder) throws Exception {
      builder.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
   }

   @Override
   protected void configure(HttpSecurity security) throws Exception {
      String [] nonCsrfUrls = new String [] {
         ConstantsHelper.MAPPING_REPORTS_UPLOAD
      };
        
      security
         .headers()
            .frameOptions().disable();

      security
        .csrf()
          .ignoringAntMatchers(nonCsrfUrls);

      security
         .authorizeRequests()
            .antMatchers(ConstantsHelper.MAPPING_LOGIN).permitAll()
            .antMatchers(HttpMethod.GET, ConstantsHelper.MAPPING_SIGNUP).permitAll()
            .antMatchers(HttpMethod.POST, ConstantsHelper.MAPPING_SIGNUP).permitAll()
            .antMatchers(HttpMethod.GET, ConstantsHelper.MAPPING_SIGNUP_PERSONALINFO).permitAll()
            .antMatchers(HttpMethod.GET, ConstantsHelper.MAPPING_SIGNUP_AVATARS).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_STYLES).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_IMAGES).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_JQUERY).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_JQUERYADDRESS).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_PUBLIC_AJAX).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_PUBLIC_JAVASCRIPTS).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_SEMANTICUI).permitAll()
            .antMatchers(ConstantsHelper.SECURITY_EXCLUSION_SCRUMIFYJS).permitAll()
            .antMatchers("/actuator/**").permitAll()
            .antMatchers("/metrics/**").permitAll()
            .antMatchers("/prometheus/**").permitAll()
            .anyRequest().authenticated()
            .and()
         .formLogin()
            .loginPage(ConstantsHelper.MAPPING_LOGIN)
            .failureHandler(new AuthenticationFailureHandlerConfiguration())
            .successHandler(new AuthenticationSuccessHandlerConfiguration())
            .permitAll()
            .and()
         .rememberMe()
            .key(ConstantsHelper.SECURITY_REMEMBERME_KEY)
            .rememberMeParameter(ConstantsHelper.SECURITY_REMEMBERME_PARAMETER)
            .rememberMeCookieName(ConstantsHelper.SECURITY_REMEMBERME_COOKIE)
            .tokenValiditySeconds(ConstantsHelper.SECURITY_REMEMBER_ME_TOKEN_VALIDITY)
            .and()
         .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher(ConstantsHelper.MAPPING_LOGOUT))
            .invalidateHttpSession(true)
            .permitAll()
         ;
   }
}
