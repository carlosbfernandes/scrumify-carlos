package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.Year;

public interface TimesheetReviewService {
   TimesheetReview getOne(Integer id);
   TimesheetReview getOne(Resource resource, Year year, Integer month, Integer fortnight, String code, String description);
   TimesheetReview save(TimesheetReview entity);
   void delete(Integer id);
   List<TimesheetReview> getByTimesheet(Timesheet timesheet);
}